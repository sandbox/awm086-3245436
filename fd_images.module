<?php

/**
 * @file
 * Contains fd_images.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_help().
 */
function fd_images_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the fd_images module.
    case 'help.page.fd_images':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provide users ability to add images to field descriptions.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function fd_images_field_widget_form_alter(&$element, \Drupal\Core\Form\FormStateInterface $form_state, $context) {

  /* @var \Drupal\field\Entity\FieldConfig $field */
  $field = $context['items']->getFieldDefinition();
  if ($field instanceof \Drupal\field\Entity\FieldConfig ){
    $name = $field->getName();
    $settings = $field->getThirdPartySettings('fd_images');
  }

  if (!empty($settings['images'])) {
    $image_element['images'] = ['#tree' => TRUE];
    foreach ($settings['images'] as $key => $path) {
      $index = $key + 1;
      $image_element['images'][$key] = [
        '#type' => 'link',
        '#title' => t(" Image $index "),
        //  '#url' => Drupal\Core\Url::fromUri('internal:/' . drupal_get_path('theme', 'adminimal_theme') . '/screenshot.png'),
        '#url' => Drupal\Core\Url::fromRoute("fd_images.image_page_controller_image", [
          'field_name' => $name,
          'settings' => ['image_link' => $path]
        ]),
        '#attributes' => [
          'class' => ['use-ajax'],
          'data-dialog-type' => 'modal',
        ],
      ];
    }
    $html = \Drupal::service('renderer')->render($image_element['images']);
    /* @var Drupal\Core\Field\FieldFilteredMarkup $new_description */
    $new_description = \Drupal\Core\Field\FieldFilteredMarkup::create(t($element['#description'] . $html));
    $element['#description'] = $new_description;
    // For some elements we need to alter this element.
    if (isset($element['value']['#description'])) {
      $element['value']['#description'] = $new_description;
    }

  }

}

/**
 * Implements hook_form_FORM_ID_alter() for 'field_config_edit_form'.
 */
function fd_images_form_field_config_edit_form_alter(array &$form, FormStateInterface $form_state) {

  // Add an optional image link on field settings form to be used as screenshot
  // on content edit page.
  $settings = $form_state->getFormObject()
    ->getEntity()
    ->getThirdPartySettings('fd_images');

  $images = [];
  if (isset($settings['images'])) {
    $images = $settings['images'];
  }
  // Gather the number of images in the form already.
  $num_images = $form_state->get('num_images') ;
  // Gather the number of existing images:
  // We have to ensure that there is at least one image field.
  if ($num_images === NULL) {
    if (count($images) > 0) {
      $num_images = count($images);
      $form_state->set('num_images', $num_images);
    }
    else {
      $form_state->set('num_images', 1);
      $num_images = 1;
    }
  }

  $form['#tree'] = TRUE;
  $element = [
    '#type' => 'fieldset',
    '#title' => t('Help images.'),
    '#description' => t('link to an image to be displayed for help on node edit form such as a screenshot, e.g. <code>/path/to/internal/image</code> or external: <code>www.example.com/mage.jpg</code>.'),
    '#prefix' => '<div id="images-fieldset-wrapper">',
    '#suffix' => '</div>',
  ];

  for ($i = 0; $i < $num_images; $i++) {
    $element['images'][$i] = [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => isset($images[$i]) ? $images[$i] : "",
    ];
  }

  $element['actions'] = [
    '#type' => 'actions',
  ];
  $element['actions']['add_image'] = [
    '#type' => 'submit',
    '#value' => t('Add one more'),
    '#submit' => ['fd_images_add_one'],
    '#ajax' => [
      'callback' => 'fd_images_add_more_callback',
      'wrapper' => 'images-fieldset-wrapper',
    ],
  ];
  // If there is more than one image, add the remove button.
  if ($num_images >= 1) {
    $element['actions']['remove_image'] = [
      '#type' => 'submit',
      '#value' => t('Remove one'),
      '#submit' => ['fd_images_remove_callback'],
      '#ajax' => [
        'callback' => 'fd_images_add_more_callback',
        'wrapper' => 'images-fieldset-wrapper',
      ],
    ];
  }

  $form['third_party_settings']['fd_images'] = $element;
  $form['third_party_settings']['#weight'] = -10;
  // Add our validation.
  $form['#validate'][] = 'fd_images_form_field_config_edit_form_validate';
}


/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the images in it.
 */
function fd_images_add_more_callback(array &$form, FormStateInterface $form_state) {
  return $form['third_party_settings']['fd_images'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function fd_images_add_one(array &$form, FormStateInterface $form_state) {
  $image_field = $form_state->get('num_images');
  $add_button = $image_field + 1;
  $form_state->set('num_images', $add_button);
  // Since our buildForm() method relies on the value of 'num_images' to
  // generate 'image' form elements, we have to tell the form to rebuild. If we
  // don't do this, the form builder will not call buildForm().
  $form_state->setRebuild();
}

/**
 * Submit handler for the "remove one" button.
 *
 * Decrements the max counter and causes a form rebuild.
 */
function fd_images_remove_callback(array &$form, FormStateInterface $form_state) {
  $image_field = $form_state->get('num_images');
  if ($image_field > 1) {
    $remove_button = $image_field - 1;
    $form_state->set('num_images', $remove_button);
  }
  // Since our buildForm() method relies on the value of 'num_images' to
  // generate 'image' form elements, we have to tell the form to rebuild. If we
  // don't do this, the form builder will not call buildForm().
  $form_state->setRebuild();
}

/*
 * Validation function for our images.
 */
function fd_images_form_field_config_edit_form_validate(array &$form, FormStateInterface $form_state) {
  // Remove the actions from the settings.
  $third_party_settings = $form_state->getValue('third_party_settings');
  unset($third_party_settings['fd_images']['actions']);
  unset($third_party_settings['fd_images']['#weight']);

  // Remove empty values.
  foreach ($third_party_settings['fd_images']['images'] as $key => $image) {
    if (empty($image)) {
      unset($third_party_settings['fd_images']['images'][$key]);
    }
  }

  $form_state->setValue('third_party_settings', $third_party_settings);
}