<?php

namespace Drupal\fd_images\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
//use Drupal\Core\Logger\LoggerChannelFactoryInterface;
//use  \Drupal\Core\Render\RendererInterface;

/**
 * Class ImagePageController.
 */
class ImagePageController extends ControllerBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

//  /**
//   * The renderer service.
//   *
//   * @var \Drupal\Core\Render\RendererInterface
//   */
//  protected $renderer;
//
//  /**
//   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
//   *
//   * @var \Drupal\Core\Logger\\LoggerChannelInterface
//   */
//  protected $logger;

  /**
   * Constructs a new ImagePageController object.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   */
  public function __construct(Request $request) {
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Returns markup to build a simple page with an embeded image.
   *
   * @return array
   *   Return Hello string.
   */
  public function image($field_name) {
    // Get the field image path from the request.
    $settings = $this->request->query->get('settings');
    // Build the markup.
    $build = [
      '#type' => 'markup',
      '#prefix' => '<div class="my-div">',
      '#suffix' => '</div>',
      '#markup' => '<img src="' . $settings['image_link']  . '" alt="example image">',

    ];
    // Attach css/js.
    $build['#attached']['library'][] = 'fd_images/drupal.fd_images';

    return $build;
  }

}
